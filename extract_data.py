from os import environ
from requests import get
from json import loads
from pandas import read_csv # tmp
from pandas import DataFrame

baserow_api_key = environ['baserow_api_key']

headers = {
    "Authorization": "Token " + baserow_api_key
}


tables_data = {
        "entreprises": {
            "url": "https://api.baserow.io/api/database/rows/table/46944/?user_field_names=true",
            "filter": "" # field__12345Activité_=123MarchéMunicipal
        },
        "evenements": {
            "url": "https://api.baserow.io/api/database/rows/table/47228/?user_field_names=true",
            "filter": ""
        },
        "presences": {
            "url": "https://api.baserow.io/api/database/rows/table/46137/?user_field_names=true",
            "filter": ""
        }

baserow_url = {
    "evenements": "",
    "entreprises": "",
    "marches-hebdomadaires": ""
}



tables = list(baserow_url.keys())

csv = {}

def get_baserow_data(baserow_url):
    return loads(get(
            baserow_url,
            headers=headers
        ).content) 

for table in tables:

    # Télécharger les données présentes dans baserow
    # Pour toutes les tables : limiter les champs demandés
    # Pour les entreprises : ajouter un filtre pour réduire le nombre
    this_table_baserow_url = tables_data[table]["url"] #+ "?" + tables_data[table]["filter"]
    csv[table] = get_baserow_data(this_table_baserow_url)
    next = csv[table]["next"]
    while != None:
        following_data = get_baserow_data(csv[table]["next"])
        csv[table]["results"].append(following_data["results"])


        next = following_data["next"]

    # Produire le JSON

    if table == "evenements":
        evenements = DataFrame(csv[table])


    #csv[table] = get(
    #    baserow_url[table],
    #    headers=headers
    #).content

    # Temporairement : lire le CSV téléchargé à la main

    csv[table] = read_csv()

    # Produire le JSON

    if table == "entreprises":
        # Activité -> catégorie "Type" (de préférence la plus basse possible dans la hiérarchie, pas de soucis si aussi les parents)

        # Statut (membre & encaissement ? pas membre) -> catégorie "Monnaie locale" (accepte les stücks - bientôt ?)

        # Cluster -> filière et chaîne d'appro et bassin, suivant le type du cluster

        # liste des fournisseurs
        # liste des clients
        # avec lien vers l'info-fiche gogocarto

        # Calculer la distance au marché le plus proche

        # Calculer la distance au bureau de change le plus proche

        pass

        elif table == "evenements":
        # type : marché (hebdo, privé, créateur·ices) ou événement (festival, etc)

        # régulalité (répétition) : hebdo/mensuelle/annuelle -> catégorie
        # jour de la régularité -> catégorie - avec un S ! ex: lundiS
        # mois de la régularité -> catégorie  - avec un S ! ex: maiS

        # Dans champs du formulaire :
        # - orga qui organise
        # - orga qui tient le lieu
        # - liste des exposants
        # et pour chacun de ces pros : lien vers sa propre info-page sur gogocarto
        pass

        # Présences :
        # date -> formulaire
        # décomposer la date en mois jour, jour de la semaine (optionnel, uniquement si module python)
        
        # type de présence (rdv - bureau de change - réunion d'information collective)
        pass
       

    # json_data = DataFrame ( csv[table]  )
    json_data.to_json(table + ".json")

# Pusher sur le repo gitlab

!git add ...tables
!git commit -m "mise à jour des données"
!git push origin master

