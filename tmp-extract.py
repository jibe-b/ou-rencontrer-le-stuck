# python3 tmp-extract.py ; git status && git add *.json && git commit -m "converted from baserow format to gogocarto" && git push origin main

from requests import get
from json import loads, dump
from datetime import datetime

def get_baserow_json_from_gitlab(url):
    return loads(get(url).content)

activites_baserow = get_baserow_json_from_gitlab("https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/activites.json")
contacts_baserow = get_baserow_json_from_gitlab("https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/contacts.json")

activity_labels = {}
for activite in activites_baserow:
    try:
        id_ = activite["id"]
        name = activite["Name"]
        type = activite["type"][0]["value"]

        category_label = type + " : " + name
        activity_labels[id_] = category_label

    except:pass

contacts_roles = {}
for contact in contacts_baserow:
    id_ = contact["id"]
    try:
        role = contact["rôle"]
        contacts_roles[id_] = "Contact : " + role
    except:
        contacts_roles[id_] = "Contact : standard"

map_data = {
    "marchés hebdomadaires": {
        "original_data_url": "https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/marches-hebdomadaires.json",
        "output_file": "marches-hebdomadaires.json"
    },
    "entreprises": {
        "original_data_url": "https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/entreprises.json",
        "output_file": "entreprises.json"
    },
    "présences": {
        "original_data_url": "https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/presences.json",
        "output_file": "presences.json"
    },
    "événements": {
        "original_data_url": "https://gitlab.com/jibe-b/ou-rencontrer-le-stuck-deamon/-/raw/main/evenements.json",
        "output_file": "evenements.json"
    }
}

mapping = {
        "name": {
            "marchés hebdomadaires": "Nom de l'événement",
            "entreprises": "Dénomination",
            "présences": "Titre",
            "événements": "Nom de l'événement"
        },
        "addressLocality": {
            "marchés hebdomadaires": "ville (déduit via orga)",
            "entreprises": "Ville",
            "présences": "Ville de l\'événement (lieu où organisé)",
            "événements": "ville (déduit via orga)"
        },
        "url": {
            "marchés hebdomadaires": "Lien",
            "entreprises": "Site web",
            "présences": "Titre",
            "événements": "Lien"
        },
        "date": {
            "présences": "Date fixée",
            "événements": "Date"
        }
}

# parent_activities = { "1": []}


def parse_fields(types_of_points, item, el):
    try:    name = item[ mapping["name"][types_of_points] ]
    except: name = "no name"
    el["name"] = name

    el["url"] = item[ mapping["url"][types_of_points] ]

    # geo
    try:
        latitude = float(item["Latitude géo"][0]["value"])
        longitude = float(item["Longitude géo"][0]["value"])

        if (item["Latitude géo"][0]["value"] != None) & (item["Longitude géo"][0]["value"] != None) :
            el["geo"] = {
                "latitude": latitude,
                "longitude": longitude
            }
    except:pass
    try:
        el["adress"] = {
            "addressLocality": item[ mapping["addressLocality"][types_of_points] ][0]["value"]
            }
    except:pass

    # categories
    el["categories"] = []
    el["categories"].append("Fournisseur de : " + name) #("Lui-même de : " + name)
    try: el["categories"].append("<5km de : " + item["Marché hebdo le plus proche"][0]["value"])
    except: pass

    try:
        for cluster in item["ClusterS-chaîneS d'appro"]:
            el["categories"].append(cluster["value"])
    except: pass

    if types_of_points == "marchés hebdomadaires":
            try:
                formats = [format["value"] for format in item["format"]]
                mapping_catégories_de_marchés = {
                        "marché municipal": "marché municipal",
                        "marché privé": "marché privé",
                        "mini-marché privé": "mini-marché privé"
                }
        
                for catégorie_de_marché in list(mapping_catégories_de_marchés.keys()):
                    if catégorie_de_marché in formats:
                        el["categories"].append(mapping_catégories_de_marchés[catégorie_de_marché])
            except: pass

            try:
                regularites = ["hebdomadaire", "mensuelle", "annuelle", "ponctuelle", "permanente"]
                jours = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
                repetition = [repetition["value"] for repetition in item['Régularité et jour']]
                for regularite in regularites:
                    if regularite in repetition:
                        el["categories"].append("Répétition : " + regularite)
                for jour in jours:
                    if jour in repetition:
                        el["categories"].append("Jour de répétition : " + jour)
                for mois in ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]:
                    el["categories"].append("Mois de répétition : " + mois)
        # Années, par défaut toutes les années à venir
                années = ["2022", "2023"]
                for année in années:
                    el["categories"].append("Année de répétition : " + année) 
            except: pass

    elif types_of_points == "entreprises":
        el["categories"].append("Répétition : " + "Permanente") # Sauf Ravito, Grenze…

        try:
            for marché in item["Marché à proximité de la ville"]:
                el["categories"].append("À proximité du marché : " + marché["value"])
        except:pass


        for activite in item["Activité (normalisée)"]:
            try:
                id_ = activite["id"]
                el["categories"].append(activity_labels[id_])
                # for parent_activity in parent_activities[id_]:
                    # el["categories"].append(parent_activity)
            except: pass
    
        contacts_ids = []
        for contact in item["Contact dans l\'entreprise"]:
            id_ = contact["id"]
            el["categories"].append(contacts_roles[id_])
            contacts_ids.append(str(id_))
        el["contact id"] = ",".join(contacts_ids)

        link_mapping = {
                'NOMS - fournisseur de (a pour clients)': "Fournisseur de",
                'se fournit chez (client de) (via proxy)': "Client de",
                'Noms des structures dont est membre': "Membre de",
                "📆 Événements - Buvette/food-truck": "Exposant sur",
                'Accepte moyens de paiement': "Accepte"
        }

        for link_type in list(link_mapping.keys()):
                link_field = ""
                for link in item[link_type]:
                    el["categories"].append(link_mapping[link_type] + " : " + link["value"])

                    link_field += link["value"] + ", "
                el[link_mapping[link_type]] = link_field
    
    elif types_of_points in ["présences", "événements"]:
        mois_en_toutes_lettres = {"01":"janvier","02":"février","03":"mars","04":"avril","05":"mai","06":"juin","07":"juillet","08":"août","09":"septembre","10":"octobre","11":"novembre","12":"décembre"}
        jours_de_la_semaine = {"0":"lundi","1":"mardi","2":"mercredi","3":"jeudi","4":"vendredi","5":"samedi","6":"dimanche"}


        try:
            date = item[ mapping["date"][types_of_points] ]
            el["date"] = date
            date_full = datetime.strptime(date, "%Y-%m-%d")
            année = date_full.strftime("%Y")
            mois = mois_en_toutes_lettres[date_full.strftime("%m")]
            numero_du_jour = date_full.strftime("%d")
            if "0" in numero_du_jour:
                numero_du_jour = numero_du_jour.replace("0", "")

            jour_de_la_semaine =  jours_de_la_semaine [ str(date_full.weekday())]
            for category in [année, année, mois, numero_du_jour, jour_de_la_semaine]:
                el["categories"].append(category)

        # puis ajouter la date au début du nom

            if types_of_points == "événements":
                el["categories"].append("événement")
                for format in item["format"]:
                    el["categories"].append("format : " + format["value"])
        except: pass

    return el

for types_of_points in list(map_data.keys()):
        map_data[types_of_points]["data"] = get_baserow_json_from_gitlab(map_data[types_of_points]["original_data_url"])

        els = {"ontology":"gogofull", "data":[]}

        for item in map_data[types_of_points]["data"]:
            el = {"categories": []}
            parsed = parse_fields(types_of_points, item, el)
           
            try: el["name"] = parsed["name"]
            except: pass
            try:
                el["adress"] = parsed["adress"]
            except:
                pass
            try: el["date"] = parsed["date"]
            except: pass
            
            el["categories"] = el["categories"] + parsed["categories"] 


            els["data"].append(el)
        map_data[types_of_points]["els"] = els
        dump(els, open(map_data[types_of_points]["output_file"], 'w', encoding='utf8'), ensure_ascii=False)



batch_length = 400
offset = 0
iteration = 1
els_to_dump = loads(open("entreprises.json", 'r').read())
while offset < len(els_to_dump["data"]):
            file_name = "entreprises-" + str(iteration) + ".json"
            els_selected = els_to_dump.copy()
            els_selected["data"] = els_to_dump["data"][offset:offset + batch_length]
            print(file_name)
            dump(els_selected, open(file_name, 'w'))
            offset += batch_length
            iteration += 1


